

describe('randomService', () => {
  test('randomService gets a random number', async () => {
    const randomService = require('./randomService');
    const rs = new randomService();
    expect(rs.getRandomNumber()).toEqual(2);
  }, 5000);
});
